const express=require('express');
const _=require('underscore');
const bodyParser=require('body-parser');
const db=require('../database/db');
const genError=require('./gene-error');

const router=module.exports=express.Router();
router.use(bodyParser.json());

router.post('/',
	(req,res,next)=>
		db.insert(req.body)
		.then(()=>res.sendStatus(201))
		.catch(next)
);
router.get('/nodes',
	(req,res,next)=>
		db.getNodes()
		.then(rows=>res.json(rows))
		.catch(next)
);
router.get('/nodes/:node',
	(req,res,next)=>{
		if(!req.query.start)
			return next(genError(400,'请求必须有start查询参数'));
		var startTime = null;
		var endTime = null;
		try{
			startTime=new Date(req.query.start);
			endTime=req.query.end?new Date(req.query.end):new Date();
		}
		catch(e){next(e);}

		db.getData(req.params.node,startTime,endTime)
		.then(
			rows=>{
				if(rows.length === 0) throw genError(404,'No data on that day');
				return res.json(rows);
			}
		)
		.catch(next);
	}
);
router.get('/nodes/:node/pktloss',
	(req,res,next)=>{
		if(!req.query.start)
			return next(genError(400,'请求必须有start查询参数'));
		var startTime = null;
		var endTime = null;
		try{
			startTime=new Date(req.query.start);
			endTime=req.query.end?new Date(req.query.end):new Date();
		}
		catch(e){next(e);}

		db.getPacketLossData(req.params.node,startTime,endTime)
		.then(
			rows=>{
				if(rows.length === 0) throw genError(404,'No data on that day');
				return res.json(rows);
			}
		)
		.catch(next);
	}
);
router.get('/nodes/:node/latency',
	(req,res,next)=>{
		if(!req.query.start)
			return next(genError(400,'请求必须有start查询参数'));
		var startTime = null;
		var endTime = null;
		try{
			startTime=new Date(req.query.start);
			endTime=req.query.end?new Date(req.query.end):new Date();
		}
		catch(e){next(e);}

		db.getLatencyData(req.params.node,startTime,endTime)
		.then(
			rows=>{
				if(rows.length === 0) throw genError(404,'No data on that day');
				return res.json(rows);
			}
		)
		.catch(next);
	}
);
router.get('/nodes/:node/latest', (req,res,next)=>{
	db.getLatest(req.params.node)
	.then(d=>res.json(d))
	.catch(next)
})

router.get('/nodes/:node/onlinerate', (req, res, next)=>{
	db.getPacketLossData(req.params.node, new Date('1970-01-01'))
	.then(rows=>{
		if(rows.length === 0) throw genError(404,'No such node or data');
		totoalPackets = 100 * rows.length;
		totoalReceived = rows.map(r=>r.pktloss*100).reduce((a,b)=>a+b,0);
		res.json({onlinerate:totoalReceived/totoalPackets});
	})
	.catch(next)
});

router.delete('/',(req,res)=>{
	res.sendStatus(200);
	process.exit();
});
