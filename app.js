const Promise=require('bluebird');
const express=require('express');
const config=require('./config/config');
const db=require('./database/db');
const router=require('./components/router');
const errorHandler=require('./components/error-handler');
const sendready=require('./config/sendready');
const fs=Promise.promisifyAll(require('fs'));

const app=express();
app.use(router);
app.use(errorHandler);

Promise.resolve()
.then(
	()=>config.load()
)
.then(
	()=>db.init()
)
.then(()=>
	fs.accessAsync(config.get('serverSocket'),fs.constants.F_OK)
	.then(
		()=>fs.unlinkAsync(config.get('serverSocket')),
		err=>Promise.resolve()
	)
	.then(()=>{
		db.startAutoclean();
		app.listen(config.get('serverSocket'));
	})
)
.then(
	()=>sendready('DataManager')
);

//Does nothing when receiving SIGQUIT
process.on('SIGQUIT',()=>'');
