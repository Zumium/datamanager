/*
本文件包含了各种数据管理所需的SQL语句及其执行代码
包括查询和添加纪录
查询包括查询丢包率和延迟
本模块同时包含了定时清除程序
该程序定时清除时间过久的数据，降低数据存储对系统资源的占用
*/
const Promise = require('bluebird');
const _ = require('underscore');
const sqlite = Promise.promisifyAll(require('sqlite3'));
const config = require('../config/config');
const util = require('util');
const genError = require('../components/gene-error')

var db = null;
var cleaner = null; //存储timer
/*
 * 初始化并打开数据库
 */
/*
 * 建表SQL
 * RECORD_TBLNAME 为表名
 * CRT_RECORDTBL 为建表SQL语句
 */
const RECORD_TBLNAME = 'Records';
const CRT_RECORDTBL = `CREATE TABLE IF NOT EXISTS ${RECORD_TBLNAME}(
	ID       INTEGER PRIMARY KEY,
	PKTLOSS  REAL NOT NULL,
	LATENCY  REAL NOT NULL,
	ADDR     TEXT NOT NULL,
	IFACE    TEXT NOT NULL,
	RCDTIME  INTEGER NOT NULL);`;
/*
 * 自动缩减数据库文件大小
 */
const AUTOVACUUM_SQL = 'PRAGMA auto_vacuum = FULL;';
/*
 * 初始化数据库
 */
exports.init = () => new Promise(
    (resolve, reject) => {
        db = new sqlite.Database(config.get('databaseFile'), err => {
            if (err) return reject(err);
            db.runAsync(AUTOVACUUM_SQL)
            db.runAsync(CRT_RECORDTBL)
                .then(resolve)
                .catch(reject);
        });
    }
);

/*
 * 插入新纪录
 */
exports.insert = record => new Promise(
    (resolve, reject) => {
        const INSERT_SQL = `INSERT INTO ${RECORD_TBLNAME}(ADDR,IFACE,LATENCY,PKTLOSS,RCDTIME) VALUES(?,?,?,?,?);`;
        var recordfilted = _.pick(record, 'address', 'interface', 'latency', 'pktloss');
        db.runAsync(INSERT_SQL, recordfilted['address'], recordfilted['interface'], recordfilted['latency'], recordfilted['pktloss'], Date.now())
            .then(resolve, reject);
    }
);
/*
 * 清除过期数据
 */
const cleanExpire = () => {
        const CLNEXPR_SQL = `DELETE FROM ${RECORD_TBLNAME} WHERE RCDTIME < ?`;
        var time = new Date();
        time.setDate(time.getDate() - config.get('expireTime'));
        return db.runAsync(CLNEXPR_SQL, time.getTime());
    }
    /*
     * 启动定时清理程序
     */
exports.startAutoclean = () => {
    if (!cleaner)
        cleaner = setInterval(cleanExpire, 30 * 60 * 1000);
};
/*
 * 关闭定时清理程序
 */
exports.stopAutoclean = () => {
    if (cleaner) {
        clearInterval(cleaner);
        cleaner = null;
    }
};
/*
 * 读取延时和丢包率数据
 */
exports.getData = (addr, start, end = new Date()) => {
    if (!util.isString(addr) || !util.isDate(start) || !util.isDate(end))
        return Promise.reject(new Error('Wrong argument type! Address should be remote\' address and start,end should be a Date object'));
    const DATA_QUERY_SQL = `
		SELECT PKTLOSS,LATENCY,RCDTIME FROM ${RECORD_TBLNAME} WHERE ADDR= ? AND RCDTIME >= ? AND RCDTIME < ?;
	`;
    return db.allAsync(DATA_QUERY_SQL, addr, start.getTime(), end.getTime())
        .then(rows => rows.map(
            row => {
                row.time = new Date(row.RCDTIME);
                row.pktloss = row.PKTLOSS;
                row.latency = row.LATENCY;
                return _.pick(row, 'pktloss', 'latency', 'time');
            }
        ));
};
/*
/*
 * 读取丢包率数据
 */
exports.getPacketLossData = (addr, start, end = new Date()) => {
    if (!util.isString(addr) || !util.isDate(start) || !util.isDate(end))
        return Promise.reject(new Error('Wrong argument type! Address should be remote\' address and start,end should be a Date object'));
    const PKTLOSS_QUERY_SQL = `
		SELECT PKTLOSS,RCDTIME FROM ${RECORD_TBLNAME} WHERE ADDR = ? AND RCDTIME >= ? AND RCDTIME < ?;
	`;
    return db.allAsync(PKTLOSS_QUERY_SQL, addr, start.getTime(), end.getTime())
        .then(rows => rows.map(
            row => {
                row.time = new Date(row.RCDTIME);
                row.pktloss = row.PKTLOSS;
                return _.pick(row, 'pktloss', 'time');
            }
        ));
};
/*
 * 获取延迟数据
 */
exports.getLatencyData = (addr, start, end = new Date()) => {
    if (!util.isString(addr) || !util.isDate(start) || !util.isDate(end))
        return Promise.reject(new Error('Wrong argument type! Address should be remote\' address and start,end should be a Date object'));
    const LATENCY_QUERY_SQL = `
		SELECT LATENCY,RCDTIME FROM ${RECORD_TBLNAME} WHERE ADDR = ? AND RCDTIME >= ? AND RCDTIME < ?;
	`;
    return db.allAsync(LATENCY_QUERY_SQL, addr, start.getTime(), end.getTime())
        .then(rows => rows.map(
            row => {
                row.time = new Date(row.RCDTIME);
                row.latency = row.LATENCY;
                return _.pick(row, 'latency', 'time');
            }
        ));
};
/*
 * 获取节点信息
 */
exports.getNodes = () => {
        const GET_NODES_SQL = `SELECT DISTINCT ADDR,IFACE FROM ${RECORD_TBLNAME};`;
        return db.allAsync(GET_NODES_SQL)
            .then(rows => rows.map(
                row => {
                    row['address'] = row.ADDR;
                    row['interface'] = row.IFACE;
                    return _.pick(row, 'address', 'interface');
                }
            ));
    }
    /*
     * 读取某节点的最新数据
     */
exports.getLatest = addr => {
        if (!util.isString(addr))
            return Promise.reject(new Error('wrong argument type'));
        const GET_LATEST_SQL = `SELECT LATENCY,PKTLOSS FROM ${RECORD_TBLNAME} WHERE ADDR = ? ORDER BY RCDTIME DESC LIMIT 1;`;
        return db.getAsync(GET_LATEST_SQL, addr)
						.then(row=>{
							if (!row)
								throw genError(404,'No such node or records');
							return {latency: row.LATENCY, pktloss: row.PKTLOSS};
						});
    }
    /*
     * 测试代码
     * 插入数据库后读取最后删除表
     *
     * 留作参考使用
     *exports.testReadAndWrite=()=>new Promise(
     *	(resolve,reject)=>{
     *		db.runAsync(`INSERT INTO ${RECORD_TBLNAME}(PKTLOSS,LATENCY,ADDR,IFACE) VALUES(0.1,0.1,'192.168.56.101','vboxnet0');`)
     *		.then(()=>db.getAsync(`SELECT * FROM ${RECORD_TBLNAME};`))
     *		.then(row=>console.log(row))
     *		.then(()=>db.runAsync(`DROP TABLE ${RECORD_TBLNAME};`))
     *		.then(resolve)
     *		.catch(reject);
     *	}
     *);
     */
