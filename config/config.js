const request=require('request'); 

const Promise=require('bluebird');
const mainModuleSock=process.argv[process.argv.length-1];

var config={};

exports.load=()=>new Promise((resolve,reject)=>
	request
	.get('http://unix:'+mainModuleSock+':/config/DataManager',
		(error,response,body)=>{
			if(error || response.statusCode !== 200)
				return reject(new Error('Config requesting failed'));
			config=JSON.parse(body);
			resolve(config);
		}
	)
);

exports.get=paramName=>config[paramName];
